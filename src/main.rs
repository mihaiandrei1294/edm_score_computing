#![allow(dead_code)]
extern crate strum;
extern crate strum_macros;

pub mod regula;
pub mod data_reader;
pub mod score_computing;
pub mod radrar;

use radrar::{ConfPozitive, ConfNegative};

fn main() {
    // score_computing::main();
    radrar::main(ConfPozitive::C098, ConfNegative::C0995);
}
