use std::io::{BufReader};
use std::io::prelude::*;
use std::fs::File;

use regula::{Regula, Comparant, Comparatie};

pub fn read_reguli(filename:&'static str) -> Vec<Regula>{
    let f = File::open(filename).expect("file not found");
    let f = BufReader::new(f);

    let mut rez =Vec::new();

    for line in f.lines() {
        let line_str:String = line.unwrap();
        let components:Vec<&str> = line_str.split_whitespace().collect();
        if components.len() == 4{
            let regula = Regula::L1 {
                comparant1: parse_comparant(components[0]),
                comparatie: parse_comparatie(components[1]),
                comparant2: parse_comparant(components[2]),
                confidenta: components[3].parse().unwrap(),
            };
            rez.push(regula);
        } else if components.len() == 6{
            let regula = Regula::L2 {
                comparant1: parse_comparant(components[0]),
                comparatie1: parse_comparatie(components[1]),
                comparant2: parse_comparant(components[2]),
                comparatie2: parse_comparatie(components[3]),
                comparant3: parse_comparant(components[4]),
                confidenta: components[5].parse().unwrap(),

            };
            rez.push(regula);
        } else if components.len() == 8{
            let regula = Regula::L3 {
                comparant1: parse_comparant(components[0]),
                comparatie1: parse_comparatie(components[1]),
                comparant2: parse_comparant(components[2]),
                comparatie2: parse_comparatie(components[3]),
                comparant3: parse_comparant(components[4]),
                comparatie3: parse_comparatie(components[5]),
                comparant4: parse_comparant(components[6]),
                confidenta: components[7].parse().unwrap(),
            };
            rez.push(regula);
        }else if components.len() == 10{
            let regula = Regula::L4 {
                comparant1: parse_comparant(components[0]),
                comparatie1: parse_comparatie(components[1]),
                comparant2: parse_comparant(components[2]),
                comparatie2: parse_comparatie(components[3]),
                comparant3: parse_comparant(components[4]),
                comparatie3: parse_comparatie(components[5]),
                comparant4: parse_comparant(components[6]),
                comparatie4: parse_comparatie(components[7]),
                comparant5: parse_comparant(components[8]),
                confidenta: components[9].parse().unwrap(),
            };
            rez.push(regula);
        }else if components.len() == 12{
            let regula = Regula::L5 {
                comparant1: parse_comparant(components[0]),
                comparatie1: parse_comparatie(components[1]),
                comparant2: parse_comparant(components[2]),
                comparatie2: parse_comparatie(components[3]),
                comparant3: parse_comparant(components[4]),
                comparatie3: parse_comparatie(components[5]),
                comparant4: parse_comparant(components[6]),
                comparatie4: parse_comparatie(components[7]),
                comparant5: parse_comparant(components[8]),
                comparatie5: parse_comparatie(components[9]),
                comparant6: parse_comparant(components[10]),
                confidenta: components[11].parse().unwrap(),
            };
            rez.push(regula);
        } else {
            panic!("Regula cu numar ciudat de componenete!! ({})", components.len());
        }

    }

    rez
}

fn parse_comparatie(text:&str) -> Comparatie{
    if text == "<"{return Comparatie::Lesser}
    if text == "<="{return Comparatie::LesserOrEqual}
    if text == ">"{return Comparatie::Grater}
    if text == ">="{return Comparatie::GreaterOrEqual}
    panic!("De returnat o comparatir nevalida!!! Comparatia: {}", text);
}

fn parse_comparant(text:&str) -> Comparant{
    // let componente:Vec<&str> = text.split("").collect();
    // if componente.len() > 3{
    //     let index:usize = componente[2].parse().unwrap();
    //     return Comparant::Index(index-1);
    // }
    // let nota = componente[1].parse().unwrap();
    // Comparant::Valoare(nota)

    if text.starts_with('a'){
        let index = &text[1..];
        return Comparant::Index(index.parse::<usize>().unwrap() - 1);
    }
    Comparant::Valoare(text.parse().unwrap())
}


pub fn read_note(filename:&str) -> Vec<Box<[f64]>>{
    let f = File::open(filename).expect("file not found");
    let f = BufReader::new(f);

    let mut rez = Vec::new();

    for line in f.lines() {
        let line_str:String = line.unwrap();

        let mut note = Vec::new();
        let components:Vec<&str> = line_str.split_whitespace().collect();
        for component in components{
            let rez = component.parse::<f64>();
            if rez.is_err(){
                println!("component = {}", component);
            }
            note.push(component.parse().unwrap());
        }
        rez.push(note.into_boxed_slice());
    }


    rez
}

pub fn read_date_radar(filename:&str) -> Vec<Box<[f64]>> {
    let f = File::open(filename).expect("file not found");
    let f = BufReader::new(f);

    let mut rez = Vec::new();

    for line in f.lines().map(|l| l.unwrap()) {
        let linevec: Vec<f64> = line.split_whitespace().map(|x| x.parse::<f64>().expect(&format!("cannot parse {}", x))).collect();
        if linevec.is_empty() { continue; }
        rez.push(linevec.into_boxed_slice());
    }


    rez
}
