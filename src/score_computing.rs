use crate::regula;
use crate::data_reader;
use std::f64::consts;

pub fn main() {
    let reguli_nepromovati = data_reader::read_reguli("src/data/reguli_nepromovat_mic.txt");
    println!("Citit reguli nepromovati!");
    let reguli_promovati = data_reader::read_reguli("src/data/reguli_promovat_mic.txt");
    println!("Citit reguli promovati!");
    let studenti_train = data_reader::read_note("src/data/set_date_mic.txt");
    println!("Citit studenti training!");

    let mut true_pozitives = 0.0;
    let mut true_negatives = 0.0;
    let mut false_pozitives = 0.0;
    let mut false_negatives = 0.0;
    let mut medie_reguli_note = [(0.0, 0.0, 0.0); 10];
    for student in &studenti_train{
//        println!("Scor: {}, Nota finala: {}", scor_patratic(student, &reguli_promovati, &reguli_nepromovati), student[student.len()-1]);
        let scor_plus = scor_probabilitate_plus_distanta(student, &reguli_promovati, &reguli_nepromovati);
//        let scor_minus = -scor_plus;
//        let scor_minus = 1.0 - scor_plus;
        let scor_minus = scor_probabilitate_minus_distanta(student, &reguli_promovati,&reguli_nepromovati);
//        let scor_plus = cate_reguli_valideaza(student, &reguli_promovati);
//        let scor_minus = cate_reguli_valideaza(student, &reguli_nepromovati);

//        let scor = scor_naiv_cu_reguli_partiale(student, &reguli_promovati,&reguli_nepromovati);

        let nota_finala = student[student.len()-1];
        println!("Scor+: {}, Scor-: {}, Nota finala: {} \n",
                 scor_plus, scor_minus, nota_finala);
//        if (student[student.len() -1] - 5.0).abs() < 0.05{continue}

        medie_reguli_note[nota_finala as usize-1].0 += scor_plus;
        medie_reguli_note[nota_finala as usize-1].1 += scor_minus;
        medie_reguli_note[nota_finala as usize-1].2 += 1.0;

        let promovat_real = student[student.len()-1] >= 6.0;
        let promovat_prezis = scor_plus >= 0.5;
        let promovat_prezis = scor_plus >= scor_minus;


        if promovat_real{
            if promovat_prezis{
                true_pozitives += 1.0;
            } else {
                false_negatives += 1.0;
            }
        } else {
            if promovat_prezis{
                false_pozitives += 1.0;
            } else {
                true_negatives += 1.0;
            }
        }
    }

    for i in 0..medie_reguli_note.len(){
        let medie_plus = medie_reguli_note[i].0/medie_reguli_note[i].2;
        let medie_minus = medie_reguli_note[i].1/medie_reguli_note[i].2;
        println!("Pentru nota {} media scorului plus e {}", i+1, medie_plus);
        println!("Pentru nota {} media scorului minus e {}", i+1, medie_minus);

    }

    let accuracy = (true_pozitives + true_negatives)/(true_pozitives+true_negatives+false_pozitives+false_negatives);
    let precision = true_pozitives/(true_pozitives+false_pozitives);
    let recall = true_pozitives/(true_pozitives + false_negatives);
    let f1 = 2.0*((precision*recall)/(precision+recall));

    println!("fp: {}, fn: {}, tp: {}, tn: {}",false_pozitives, false_negatives, true_pozitives, true_negatives);
    println!("Accuracy = {}", accuracy);
    println!("Precision = {}", precision);
    println!("Recall = {}", recall);
    println!("F1 = {}", f1);
}

// #######       SCOR 1 - naiv      ##########
// prezis promovare: scor_plus >= 0.0

fn score_naiv(note:&[f64], reguli_promovati:&Vec<regula::Regula>, reguli_nepromovati:&Vec<regula::Regula>) -> f64{
    let mut scor = 0.0;
    for r in reguli_promovati{
        if r.respecta_regula(note){
            scor += r.confidenta();
        }
    }
    for r in reguli_nepromovati{
        if r.respecta_regula(note){
            scor -= r.confidenta();
        }
    }

    scor
}


// #######       SCOR 2      ########## – scorul 3 de la ICCP
// prezis promovare: scor_plus >= 0.0

fn scor_naiv_cu_reguli_partiale(note:&[f64], reguli_promovati:&Vec<regula::Regula>, reguli_nepromovati:&Vec<regula::Regula>) -> f64{
    let mut scor_p = 0.0;
    let mut scor_m = 0.0;
    for r in reguli_promovati{
        scor_p += r.scor_respectare_a_regulii(note);
    }
    dbg!(scor_p);
    scor_p = scor_p/reguli_promovati.len() as f64;

    for r in reguli_nepromovati{
        scor_m += r.scor_respectare_a_regulii(note);
    }
    dbg!(scor_m);
    scor_m = scor_m/reguli_nepromovati.len() as f64;

    dbg!(scor_p);
    dbg!(scor_m);
    scor_p - scor_m
}


// #######       SCOR 3      ########## (scor 1 in ICCP cu reguli partiale)
// prezis promovare: scor_plus >= 0.5

fn scor_probabilitate_plus(note:&[f64], reguli_promovati:&Vec<regula::Regula>, reguli_nepromovati:&Vec<regula::Regula>) -> f64{
    let mut suma_conf_plus = 0.0;
    let mut suma_verificata_plus = 0.0;
    for r in reguli_promovati{
        suma_conf_plus += r.confidenta();
        suma_verificata_plus += r.cat_respecta_regula(note);
//        if r.respecta_regula(note) {
//            suma_verificata_plus += r.confidenta();
//        }
    }
    let mut suma_conf_minus = 0.0;
    let mut suma_nerespectare_reguli = 0.0;
    for r in reguli_nepromovati{
        suma_conf_minus += r.confidenta();
        suma_nerespectare_reguli += r.confidenta() - r.cat_respecta_regula(note);
//        if !r.respecta_regula(note){
//            suma_nerespectare_reguli += r.confidenta();
//        }
    }
    println!("n+ = {}, n- = {}", suma_conf_plus, suma_conf_minus);
    println!("v+ = {}, nv- = {}", suma_verificata_plus, suma_nerespectare_reguli);
    (suma_verificata_plus + suma_nerespectare_reguli)/(suma_conf_minus + suma_conf_plus)
}

fn scor_probabilitate_minus(note:&[f64], reguli_promovati:&Vec<regula::Regula>, reguli_nepromovati:&Vec<regula::Regula>) -> f64{
    let mut suma_conf_minus = 0.0;
    let mut suma_verificata_minus = 0.0;
    for r in reguli_nepromovati{
        suma_conf_minus += r.confidenta();
        suma_verificata_minus += r.cat_respecta_regula(note)
//        if r.respecta_regula(note) {
//            suma_verificata_minus += r.confidenta();
//        }
    }
    let mut suma_conf_plus = 0.0;
    let mut suma_nerespectare_reguli = 0.0;
    for r in reguli_promovati{
        suma_conf_plus += r.confidenta();
        suma_nerespectare_reguli += r.confidenta() - r.cat_respecta_regula(note);
//        if !r.respecta_regula(note){
//            suma_nerespectare_reguli += r.confidenta();
//        }
    }
    println!("v- = {}, nv+ = {}", suma_verificata_minus, suma_nerespectare_reguli);
    (suma_verificata_minus + suma_nerespectare_reguli)/(suma_conf_plus + suma_conf_minus)
}

//SCOR 2 in ICCP
fn scor_probabilitate_minus_distanta(note:&[f64], reguli_promovati:&Vec<regula::Regula>, reguli_nepromovati:&Vec<regula::Regula>) -> f64{
    let mut vec_plus_scor_ideal = Vec::new();
    let mut vec_minus_scor_ideal = Vec::new();

    let mut vec_plus_scor_real = Vec::new();
    let mut vec_minus_scor_real = Vec::new();
//    let mut vec_real = Vec::new();

    for r in reguli_promovati{
        vec_plus_scor_ideal.push(r.confidenta());
//        vec_plus_scor_ideal.push(-1.0/(r.confidenta().ln()));
//        vec_plus_scor_ideal.push((5.0*r.confidenta()).exp());
//        vec_plus_scor_real.push(r.scor_respectare_a_regulii(note));
        vec_plus_scor_real.push(r.scor_respectare_a_regulii(note));
//        if r.respecta_regula(note){
//            vec_real.push(r.confidenta());
//        } else {
//            vec_real.push(0.0);
//        }
//        vec_plus_scor_real.push(-1.0/(r.scor_respectare_a_regulii(note)).ln());
//        vec_plus_scor_real.push((5.0*r.scor_respectare_a_regulii(note)).exp());
//        vec_minus_scor_ideal.push(0.0)
    }

    for r in reguli_nepromovati {
        vec_minus_scor_ideal.push(r.confidenta());
//        vec_minus_scor_ideal.push(-1.0 / (r.confidenta().ln()));
//        vec_minus_scor_ideal.push((5.0*r.confidenta()).exp());
//        vec_minus_scor_real.push(r.scor_respectare_a_regulii(note));
        vec_minus_scor_real.push(r.scor_respectare_a_regulii(note));
//        if r.respecta_regula(note){
//            vec_real.push(r.confidenta());
//        } else {
//            vec_real.push(0.0);
//        }
//        vec_minus_scor_real.push(-1.0 / (r.scor_respectare_a_regulii(note)).ln());
//        vec_minus_scor_real.push((5.0*r.scor_respectare_a_regulii(note)).exp());
//        vec_plus_scor_ideal.push(0.0);
    }
//    println!("vector minus scor ideal")

    let d_plus = euclidian_distance(&vec_plus_scor_real, &vec_plus_scor_ideal);
    let d_minus = euclidian_distance(&vec_minus_scor_ideal, &vec_minus_scor_real);

    dbg!(d_plus);
    dbg!(d_minus);

    1.0 - d_minus/(d_plus+d_minus)
}

fn scor_probabilitate_plus_distanta(note:&[f64], reguli_promovati:&Vec<regula::Regula>, reguli_nepromovati:&Vec<regula::Regula>) -> f64{
    let mut vec_plus_scor_ideal = Vec::new();
    let mut vec_minus_scor_ideal = Vec::new();

    let mut vec_plus_scor_real = Vec::new();
    let mut vec_minus_scor_real = Vec::new();

    for r in reguli_promovati{
        vec_plus_scor_ideal.push(r.confidenta());
//        vec_plus_scor_ideal.push(-1.0/(r.confidenta().ln()));
//        vec_plus_scor_ideal.push((5.0*r.confidenta()).exp());
        vec_plus_scor_real.push(r.scor_respectare_a_regulii(note));
//        vec_plus_scor_real.push(-1.0/((r.scor_respectare_a_regulii(note)).ln()));
//        vec_plus_scor_real.push((5.0*r.scor_respectare_a_regulii(note)).exp());
    }

    for r in reguli_nepromovati{
        vec_minus_scor_ideal.push(r.confidenta());
//        vec_minus_scor_ideal.push(-1.0/(r.confidenta().ln()));
//        vec_minus_scor_ideal.push((5.0*r.confidenta()).exp());
        vec_minus_scor_real.push(r.scor_respectare_a_regulii(note));
//        vec_minus_scor_real.push(-1.0/((r.scor_respectare_a_regulii(note)).ln()));
//        vec_minus_scor_real.push((5.0*r.scor_respectare_a_regulii(note)).exp());
    }
//    println!("vec- ideal = {:?}",vec_minus_scor_ideal);
//    println!("vec+ ideal = {:?}",vec_plus_scor_ideal);
//    println!("vec- real = {:?}",vec_minus_scor_real);
//    println!("vec+ real = {:?}",vec_plus_scor_real);

    let d_plus = euclidian_distance(&vec_plus_scor_ideal, &vec_plus_scor_real);
    let d_minus = euclidian_distance(&vec_minus_scor_ideal, &vec_minus_scor_real);
//    println!("d+: {}, d-: {}", d_plus, d_minus);

    dbg!(d_plus);
    dbg!(d_minus);

    1.0 - d_plus/(d_plus+d_minus)
}


// ######      SCOR simplu nota 6     #######
// prezis promovare: scor plus >= scor minus
fn cate_reguli_valideaza(note:&[f64], reguli:&Vec<regula::Regula>) -> f64 {
    let mut scor = 0.0;
    for r in reguli {
        scor += r.cat_respecta_regula(note);
    }

    scor
}

//  -----------------------    Alte scoruri --------------------------------------------------------------------------

fn scor_patratic(note:&[f64], reguli_promovati:&Vec<regula::Regula>, reguli_nepromovati:&Vec<regula::Regula>) -> f64{
    let mut scor = 0.0;
    let mut scor2 = 0.0;
    for r in reguli_promovati{
        scor += (-1.0/(r.scor_respectare_a_regulii(note).ln()))/reguli_promovati.len() as f64;
    }
    for r in reguli_nepromovati{
        scor2 += (-1.0/(r.scor_respectare_a_regulii(note).ln()))/reguli_nepromovati.len() as f64;
    }

    scor-scor2
}

fn scor_exponential(note:&[f64], reguli_promovati:&Vec<regula::Regula>, reguli_nepromovati:&Vec<regula::Regula>) -> f64{
    let mut scor = 0.0;
    let mut scor2 = 0.0;
    for r in reguli_promovati{
        scor += consts::E.powf(r.scor_respectare_a_regulii(note)+1.0)/reguli_promovati.len() as f64;
    }

    for r in reguli_nepromovati{
        scor2 += consts::E.powf(r.scor_respectare_a_regulii(note)+1.0)/reguli_nepromovati.len() as f64;
    }

    scor-scor2
}

fn score_cu_nr_reguli(note:&[f64], reguli_promovati:&Vec<regula::Regula>, reguli_nepromovati:&Vec<regula::Regula>) -> f64{
    let mut scor = 0.0;
    for r in reguli_promovati{
        if r.respecta_regula(note){
            scor += r.confidenta()/reguli_promovati.len() as f64;
        }
    }
    for r in reguli_nepromovati{
        if r.respecta_regula(note){
            scor -= r.confidenta()/reguli_nepromovati.len() as f64;
        }
    }

    scor
}

fn scor_supersimplu(note:&[f64], reguli_promovati:&Vec<regula::Regula>, reguli_nepromovati:&Vec<regula::Regula>) -> f64 {
    let mut scor = 0.0;
    for r in reguli_promovati {
        if r.respecta_regula(note) {
            scor += 1.0;
        }
    }
    for r in reguli_nepromovati {
        if r.respecta_regula(note) {
            scor -= 1.0;
        }
    }

    scor
}



fn euclidian_distance(v1:&[f64], v2:&[f64]) -> f64{
    let mut sum = 0.0;
    for i in 0..v1.len(){
        sum += (v1[i] - v2[i]).powf(2.0);
    }
    sum.sqrt()
}

fn manhattan_distance(v1:&[f64], v2:&[f64]) -> f64{
    let mut sum = 0.0;
    for i in 0..v1.len(){
        sum += (v1[i] - v2[i]).abs();
    }
    sum
}