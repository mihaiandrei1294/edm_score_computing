use crate::data_reader::{read_date_radar, read_reguli};
use crate::regula::{Regula, Comparant, Comparatie};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use std::io::{BufReader, BufWriter};
use std::io::prelude::*;
use std::fs::{File, OpenOptions, copy};

#[derive(EnumIter)]
pub enum ConfPozitive{
    C090,
    C091,
    C093,
    C095,
    C098,
}


impl ConfPozitive {
    pub fn path_fisier(&self) -> &'static str{
        match self {
            ConfPozitive::C090 => {"src/data/RARs training/R mai mare decat 35/RARs_nemaximal_0.9_8224_141_174atr(15,25,35,40,50)_R_peste_35.txt"},
            ConfPozitive::C091 => {"src/data/RARs training/R mai mare decat 35/RARs_nemaximal_0.91_6525_141_174atr(15,25,35,40,50)_R_peste_35.txt"},
            ConfPozitive::C093 => {"src/data/RARs training/R mai mare decat 35/RARs_nemaximal_0.93_3838_141_174atr(15,25,35,40,50)_R_peste_35.txt"},
            ConfPozitive::C095 => {"src/data/RARs training/R mai mare decat 35/RARs_nemaximal_0.95_1740_141_174atr(15,25,35,40,50)_R_peste_35.txt"},
            ConfPozitive::C098 => {"src/data/RARs training/R mai mare decat 35/RARs_nemaximal_0.98_181_141_174atr(15,25,35,40,50)_R_peste_35.txt"},
        }
    }
}

#[derive(EnumIter)]
pub enum ConfNegative{
    C095,
    C097,
    C098,
    C099,
    C0995,
}

impl ConfNegative {
    pub fn path_fisier(&self) -> &'static str{
        match self {
            ConfNegative::C095 => {"src/data/RARs training/R mai mic sau egal cu 35/RARs_nemaximal_0.95(mai mic, mai mare)_14803_141_174atr(15,25,35,40,50)_R_sub_35.txt"},
            ConfNegative::C097 => {"src/data/RARs training/R mai mic sau egal cu 35/RARs_nemaximal_0.97_16047_141_174atr(15,25,35,40,50)_R_sub_35.txt"},
            ConfNegative::C098 => {"src/data/RARs training/R mai mic sau egal cu 35/RARs_nemaximal_0.98_14375_141_174atr(15,25,35,40,50)_R_sub_35.txt"},
            ConfNegative::C099 => {"src/data/RARs training/R mai mic sau egal cu 35/RARs_nemaximal_0.99_11208_141_174atr(15,25,35,40,50)_R_sub_35.txt"},
            ConfNegative::C0995 => {"src/data/RARs training/R mai mic sau egal cu 35/RARs_nemaximal_0.995_2060_141_174atr(15,25,35,40,50)_R_sub_35.txt"},
        }
    }
}


//in training sunt 1321 pozitive si 19991 negative
pub fn main(conf_pozitive:ConfPozitive, conf_negative:ConfNegative){

    let reguli_pozitive = read_reguli(conf_pozitive.path_fisier());
    println!("Citit reguli pozitive!");
    let reguli_negative = read_reguli(conf_negative.path_fisier());
    println!("Citit reguli negative!");
    let date_training_pozitive = read_date_radar("src/data/RARs training/training data/train_1321 inst_141_R peste 35.txt");
    println!("Citit date_training_pozitive!");
    let date_training_negative = read_date_radar("src/data/RARs training/training data/train_19991 inst_141_R maiMicEgal 35.txt");
    println!("Citit date_training_negative!");
    // let date_testing_pozitive = read_date_radar("src/data/Testing/date_radar_t138_pozitive_R01peste35.txt");
    // println!("Citit date_testing_pozitive!");
    // let date_testing_negative = read_date_radar("src/data/Testing/date_radar_t138_negative_R01sub-egal35.txt");
    // println!("Citit date_testing_negative!");
    let toate_datele = read_date_radar("src/data/Testing/R01_date_rar_timestep_144.txt");
    println!("Citit toate_datele!");

    // let date_testing_pozitive:Vec<_> = toate_datele_138.iter().filter(|&instance| instance[0] > 35.0).collect();
    // let date_testing_negative:Vec<_> = toate_datele_138.iter().filter(|&instance| instance[0] <= 35.0).collect();

    let date_testing_pozitive:Vec<_> = toate_datele.iter().filter(|&instance| instance[0] > 35.0).map(
        |instance| instance[1..].to_vec().into_boxed_slice()).collect();
    let date_testing_negative:Vec<_> = toate_datele.iter().filter(|&instance| instance[0] <= 35.0).map(
        |instance| instance[1..].to_vec().into_boxed_slice()).collect();

    let toate_datele_138:Vec<_> = toate_datele.into_iter().map(|instance| instance[1..].to_vec().into_boxed_slice()).collect();


    creeaza_fisier_predictii_si_real(&reguli_negative, &date_testing_negative, &toate_datele_138);


    // let (medie_reguli_pozitive_respectate, stdev_pozitive) = compute_mean_and_stdev(&reguli_pozitive, &date_training_pozitive);
    let (medie_reguli_negative_respectate, stdev_negative) = compute_mean_and_stdev(&reguli_negative, &date_training_negative);

    // println!("Medie pozitive: {}", medie_reguli_pozitive_respectate);
    // println!("Stdev pozitive: {}", stdev_pozitive);
    println!("Medie negatice: {}", medie_reguli_negative_respectate);
    println!("Stdev negatice: {}", stdev_negative);

    // V1 - folosind o singură clasă
    let probabilitati_pozitive:Vec<f64> = date_testing_pozitive.iter()
            .map(|instanta| calculeaza_probabilitate(instanta, &reguli_negative, medie_reguli_negative_respectate, stdev_negative)).collect();

    println!("medie probabilitati pozitive: {}", probabilitati_pozitive.iter().sum::<f64>()/ probabilitati_pozitive.len() as f64);
    println!("true pozitives: {}", probabilitati_pozitive.iter().filter(|&p| *p<0.5).count());
    println!("false pozitives: {}", probabilitati_pozitive.iter().filter(|&p| *p>=0.5).count());

    let probabilitati_negative:Vec<f64> = date_testing_negative.iter()
            .map(|instanta| calculeaza_probabilitate(instanta, &reguli_negative, medie_reguli_negative_respectate, stdev_negative)).collect();

    println!("medie probabilitati negative: {}", probabilitati_negative.iter().sum::<f64>()/ probabilitati_negative.len() as f64);
    println!("true negatives: {}", probabilitati_negative.iter().filter(|&p| *p>=0.5).count());
    println!("false negatives: {}", probabilitati_negative.iter().filter(|&p| *p<0.5).count());


    //V2 - folosind ambele clase
    // let probabilitati_pozitive_rarpozitive:Vec<f64> = date_testing_pozitive.iter()
    //     .map(|instanta| calculeaza_probabilitate(instanta, &reguli_pozitive, medie_reguli_pozitive_respectate, stdev_pozitive)).collect();
    // let probabilitati_pozitive_rarnegative:Vec<f64> = date_testing_pozitive.iter()
    //     .map(|instanta| calculeaza_probabilitate(instanta, &reguli_negative, medie_reguli_negative_respectate, stdev_negative)).collect();
    //
    // println!("medie probabilitati pozitive rarpozitive: {}", probabilitati_pozitive_rarpozitive.iter().sum::<f64>()/ probabilitati_pozitive_rarpozitive.len() as f64);
    // println!("medie probabilitati pozitive rarnegative: {}", probabilitati_pozitive_rarnegative.iter().sum::<f64>()/ probabilitati_pozitive_rarnegative.len() as f64);
    //
    //
    // println!("true pozitives: {}", probabilitati_pozitive_rarpozitive.iter().zip(&probabilitati_pozitive_rarnegative)
    //     .filter(|(&pozitiv, &negativ)| pozitiv>negativ).count());
    // println!("false pozitives: {}", probabilitati_pozitive_rarpozitive.iter().zip(&probabilitati_pozitive_rarnegative)
    //     .filter(|(&pozitiv, &negativ)| pozitiv<=negativ).count());
    //
    // let probabilitati_negative_rarpozitive:Vec<f64> = date_testing_negative.iter()
    //     .map(|instanta| calculeaza_probabilitate(instanta, &reguli_pozitive, medie_reguli_pozitive_respectate, stdev_pozitive)).collect();
    // let probabilitati_negative_rarnegative:Vec<f64> = date_testing_negative.iter()
    //     .map(|instanta| calculeaza_probabilitate(instanta, &reguli_negative, medie_reguli_negative_respectate, stdev_negative)).collect();
    //
    // println!("medie probabilitati negative rarpozitive: {}", probabilitati_negative_rarpozitive.iter().sum::<f64>()/ probabilitati_negative_rarpozitive.len() as f64);
    // println!("medie probabilitati negative rarnegative: {}", probabilitati_negative_rarnegative.iter().sum::<f64>()/ probabilitati_negative_rarnegative.len() as f64);
    //
    // println!("true negatives: {}", probabilitati_negative_rarpozitive.iter().zip(&probabilitati_negative_rarnegative)
    //     .filter(|(&pozitiv, &negativ)| pozitiv<=negativ).count());
    // println!("false negatives: {}", probabilitati_negative_rarpozitive.iter().zip(&probabilitati_negative_rarnegative)
    //     .filter(|(&pozitiv, &negativ)| pozitiv>negativ).count());
}

fn calculeaza_probabilitate(instanta:&[f64], reguli:&[Regula], medie:f64, stdev:f64) -> f64{
    let nr = nr_reguli_respectate(&instanta, &reguli) as f64/reguli.len() as f64;
    if nr <= medie - 3.0*stdev {
        0.5*(nr/(medie - 3.0*stdev))
    } else if nr <= medie + 3.0*stdev{
        0.5+(nr - (medie - 3.0*stdev))/(12.0*stdev)
    } else {1.0}
}

fn compute_mean_and_stdev(reguli:&[Regula], date:&[Box<[f64]>]) -> (f64, f64){
    let respectare_reguli:Vec<f64> =
        date.iter().map(|instanta| nr_reguli_respectate(&instanta, &reguli) as f64/reguli.len() as f64).collect();
    let suma_reguli_resectate:f64  = respectare_reguli.iter().sum();
    let medie_reguli_respectate = suma_reguli_resectate/date.len() as f64;

    let suma_eroare_reguli:f64 =
        respectare_reguli.iter().map(|nr| (*nr as f64 - medie_reguli_respectate).powi(2)).sum();
    let stdev = (suma_eroare_reguli/date.len() as f64).sqrt();

    (medie_reguli_respectate, stdev)
}

fn nr_reguli_respectate(instanta:&[f64], reguli:&[Regula],) -> usize{
    reguli.iter().map(|regula| if regula.respecta_regula(instanta) {1} else {0}).sum()
    // let mut nr = 0;
    // for r in reguli_promovati{
    //     if r.respecta_regula(note){
    //         nr += r.confidenta();
    //     }
    // }
    // nr
}

fn creeaza_fisier_predictii_si_real(reguli:&[Regula], date_training:&[Box<[f64]>], date_testing:&[Box<[f64]>]){
//    96 linii x 222 coloane
    let (medie_reguli_respectate, stdev) = compute_mean_and_stdev(&reguli, &date_training);
    let probabilitati:Vec<f64> = date_testing.iter()
        .map(|instanta| calculeaza_probabilitate(instanta, &reguli, medie_reguli_respectate, stdev)).collect();

    let f_predicions = File::create("src/data/predictions_timestep_78_negative_rules_confidence_0.995").unwrap();
    let mut predictions_buf = BufWriter::new(f_predicions);

    let mut line = String::new();
    for (i,insance) in probabilitati.iter().enumerate(){
        if i % 222 == 0{
            line.push_str("\n");
            predictions_buf.write_all(line.as_bytes()).unwrap();
            line = String::new();
        }
        line.push_str(&insance.to_string());
        line.push(' ');
    }
}
