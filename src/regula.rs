pub enum Comparatie{
    Lesser,
    LesserOrEqual,
    GreaterOrEqual,
    Grater
}

impl Comparatie{
    pub fn compara<T:PartialOrd>(&self, n1:T, n2:T) -> bool{
        match self {
            Comparatie::Grater => n1 > n2,
            Comparatie::GreaterOrEqual => n1 >= n2,
            Comparatie::Lesser => n1 < n2,
            Comparatie::LesserOrEqual => n1 <= n2,
        }
    }
}

pub enum Comparant{
    Index(usize),
    Valoare(f64)
}

pub enum Regula{
    L1{comparant1:Comparant, comparatie:Comparatie, comparant2:Comparant, confidenta:f64},
    L2{comparant1:Comparant, comparatie1:Comparatie, comparant2:Comparant,
                             comparatie2:Comparatie, comparant3:Comparant, confidenta:f64},
    L3{comparant1:Comparant, comparatie1:Comparatie, comparant2:Comparant,
                             comparatie2:Comparatie, comparant3:Comparant,
                             comparatie3:Comparatie, comparant4:Comparant,confidenta:f64},
    L4{comparant1:Comparant, comparatie1:Comparatie, comparant2:Comparant,
                             comparatie2:Comparatie, comparant3:Comparant,
                             comparatie3:Comparatie, comparant4:Comparant,
                             comparatie4:Comparatie, comparant5: Comparant,confidenta:f64},
    L5{comparant1:Comparant, comparatie1:Comparatie, comparant2:Comparant,
                             comparatie2:Comparatie, comparant3:Comparant,
                             comparatie3:Comparatie, comparant4:Comparant,
                             comparatie4:Comparatie, comparant5: Comparant,
                             comparatie5: Comparatie, comparant6:Comparant,confidenta:f64},
}

impl Regula{
    pub fn confidenta(&self) -> f64{
        match *self {
            Regula::L1 {confidenta, ..} | Regula::L2 {confidenta, ..} | Regula::L3 {confidenta, ..} |
            Regula::L4 {confidenta, ..} | Regula::L5 {confidenta, ..}=> confidenta,
        }
    }

    pub fn cat_respecta_regula(&self, instanta:&[f64]) -> f64{
        let mut scor = 0.0;
        match self {
            Regula::L1 {comparant1,comparatie,comparant2,..} => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(nota) => nota,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(nota) => nota,
                };
                if comparatie.compara(n1, n2){scor += self.confidenta();}
                scor
            },
            Regula::L2 {comparant1,comparatie1,comparant2,comparatie2,comparant3,..} => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                if comparatie1.compara(n1, n2){scor += self.confidenta()/2.0;}
                if comparatie2.compara(n2, n3){scor += self.confidenta()/2.0;}
                scor
            },
            Regula::L3 {comparant1,comparatie1,comparant2,comparatie2,comparant3,comparatie3,comparant4,..} => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n4 = match *comparant4 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                if comparatie1.compara(n1, n2){scor += self.confidenta()/3.0;}
                if comparatie2.compara(n2, n3){scor += self.confidenta()/3.0;}
                if comparatie3.compara(n3, n4){scor += self.confidenta()/3.0;}
                scor
            },
            Regula::L4 { comparant1, comparatie1, comparant2, comparatie2, comparant3, comparatie3, comparant4, comparatie4, comparant5, .. } => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n4 = match *comparant4 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n5 = match *comparant5 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };

                if comparatie1.compara(n1, n2){scor += self.confidenta()/4.0;}
                if comparatie2.compara(n2, n3){scor += self.confidenta()/4.0;}
                if comparatie3.compara(n3, n4){scor += self.confidenta()/4.0;}
                if comparatie4.compara(n4, n5){scor += self.confidenta()/4.0;}
                scor
            }
            Regula::L5 { comparant1, comparatie1, comparant2, comparatie2, comparant3, comparatie3, comparant4, comparatie4, comparant5, comparatie5, comparant6, .. } => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n4 = match *comparant4 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n5 = match *comparant5 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n6 = match *comparant6 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };

                if comparatie1.compara(n1, n2) { scor += self.confidenta() / 5.0; }
                if comparatie2.compara(n2, n3) { scor += self.confidenta() / 5.0; }
                if comparatie3.compara(n3, n4) { scor += self.confidenta() / 5.0; }
                if comparatie4.compara(n4, n5) { scor += self.confidenta() / 5.0; }
                if comparatie5.compara(n5, n6) { scor += self.confidenta() / 5.0; }
                scor
            }
        }
    }

    pub fn scor_respectare_a_regulii(&self, instanta:&[f64]) -> f64{
        let mut scor = 0.0;
        match self {
            Regula::L1 {comparant1,comparatie,comparant2,..} => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                if comparatie.compara(n1, n2){scor += self.confidenta();}
                scor
            },
            Regula::L2 {comparant1,comparatie1,comparant2,comparatie2,comparant3,..} => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                if comparatie1.compara(n1, n2){scor += self.confidenta()/2.0;}
                if comparatie2.compara(n2, n3){scor += self.confidenta()/2.0;}
                scor
            },
            Regula::L3 {comparant1,comparatie1,comparant2,comparatie2,comparant3,comparatie3,comparant4,..} => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n4 = match *comparant4 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                if !comparatie1.compara(n1, n2){scor += self.confidenta()/3.0;}
                if !comparatie2.compara(n2, n3){scor += self.confidenta()/3.0;}
                if !comparatie3.compara(n3, n4){scor += self.confidenta()/3.0;}
                scor
            },
            Regula::L4 { comparant1, comparatie1, comparant2, comparatie2, comparant3, comparatie3, comparant4, comparatie4, comparant5, ..} => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n4 = match *comparant4 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n5 = match *comparant5 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };

                if comparatie1.compara(n1, n2){scor += self.confidenta()/4.0;}
                if comparatie2.compara(n2, n3){scor += self.confidenta()/4.0;}
                if comparatie3.compara(n3, n4){scor += self.confidenta()/4.0;}
                if comparatie4.compara(n4, n5){scor += self.confidenta()/4.0;}
                scor
            }
            Regula::L5 { comparant1, comparatie1, comparant2, comparatie2, comparant3, comparatie3, comparant4, comparatie4, comparant5, comparatie5, comparant6, .. } => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n4 = match *comparant4 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n5 = match *comparant5 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n6 = match *comparant6 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };

                if comparatie1.compara(n1, n2) { scor += self.confidenta() / 5.0; }
                if comparatie2.compara(n2, n3) { scor += self.confidenta() / 5.0; }
                if comparatie3.compara(n3, n4) { scor += self.confidenta() / 5.0; }
                if comparatie4.compara(n4, n5) { scor += self.confidenta() / 5.0; }
                if comparatie5.compara(n5, n6) { scor += self.confidenta() / 5.0; }
                scor
            }
        }
    }

    pub fn respecta_regula(&self, instanta:&[f64]) -> bool{
        match self {
            Regula::L1 {comparant1,comparatie,comparant2,..} => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                if !comparatie.compara(n1, n2){return false;}
                true
            },
            Regula::L2 {comparant1,comparatie1,comparant2,comparatie2,comparant3,..} => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                if !comparatie1.compara(n1, n2){return false;}
                if !comparatie2.compara(n2, n3){return false;}
                true
            },
            Regula::L3 {comparant1,comparatie1,comparant2,comparatie2,comparant3,comparatie3,comparant4,..} => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n4 = match *comparant4 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                if !comparatie1.compara(n1, n2){return false;}
                if !comparatie2.compara(n2, n3){return false;}
                if !comparatie3.compara(n3, n4){return false;}
                true
            },
            Regula::L4 { comparant1, comparatie1, comparant2, comparatie2, comparant3, comparatie3, comparant4, comparatie4, comparant5, .. } => {
                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n4 = match *comparant4 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n5 = match *comparant5 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                if !comparatie1.compara(n1, n2){return false;}
                if !comparatie2.compara(n2, n3){return false;}
                if !comparatie3.compara(n3, n4){return false;}
                if !comparatie4.compara(n4, n5){return false;}
                true
            }
            Regula::L5 { comparant1, comparatie1, comparant2, comparatie2, comparant3, comparatie3, comparant4, comparatie4, comparant5, comparatie5, comparant6, .. } => {

                let n1 = match *comparant1 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n2 = match *comparant2 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n3 = match *comparant3 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n4 = match *comparant4 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n5 = match *comparant5 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                let n6 = match *comparant6 {
                    Comparant::Index(i) => instanta[i],
                    Comparant::Valoare(n) => n,
                };
                if !comparatie1.compara(n1, n2){return false;}
                if !comparatie2.compara(n2, n3){return false;}
                if !comparatie3.compara(n3, n4){return false;}
                if !comparatie4.compara(n4, n5){return false;}
                if !comparatie5.compara(n5, n6){return false;}
                true
            }
        }
    }
}